#include <ioavr.h>
#include <inavr.h>
#include "W5500_driver.h"
#include "wizchip_conf.h"
#include <stdio.h>
#include <delay.h>
#include <spim.h>
#include <terminal_io.h>

extern wiz_NetInfo gWIZNETINFO;


void W5500_Reset(void)
{
  W5500_RESET_PORT &= ~W5500_RESET_PIN;
  delay_us(500);
  W5500_RESET_PORT |= W5500_RESET_PIN;
  delay_ms(1);
}

void W5500_Init(void)
{
  // ������������� SPI
  spim_init();

  // CS Pin
  W5500_CS_DDR |= W5500_CS_PIN;
  // Reset Pin
  W5500_RESET_DDR |= W5500_RESET_PIN;
  // INT Pin
  //W5500_INT_DDR &= ~W5500_INT_PIN;
  //W5500_INT_PORT |= W5500_INT_PIN;

  W5500_Reset();
}

void W5500_chipInit(void)
{
  uint8_t temp;
  uint8_t W5500FifoSize[2][8] = {{2, 2, 2, 2, 2, 2, 2, 2, }, {2, 2, 2, 2, 2, 2, 2, 2}};

  W5500DeSelect();

  /* spi function register */
  reg_wizchip_spi_cbfunc(W5500ReadByte, W5500WriteByte);

  /* CS function register */
  reg_wizchip_cs_cbfunc(W5500Select, W5500DeSelect);

  if (ctlwizchip(CW_INIT_WIZCHIP, (void*)W5500FifoSize) == -1)
    printf("W5500 initialized fail.\r\n");
  
  //check phy status
  do
  {
    if (ctlwizchip(CW_GET_PHYLINK, (void*)&temp) == -1)
    {
      printf("Unknown PHY link status.\r\n");
    }
  } while (temp == PHY_LINK_OFF);
}

void W5500WriteByte(uint8_t byte)
{
  SPI_SendRecvByte(byte);
}

uint8_t W5500ReadByte(void)
{
  return SPI_SendRecvByte(0xFF);
}

void W5500Select(void)
{
  W5500_CS_PORT &= ~W5500_CS_PIN;
}

void W5500DeSelect(void)
{
  W5500_CS_PORT |= W5500_CS_PIN;
}


