#ifndef __W5500HARDWAREDRIVER_H_
#define __W5500HARDWAREDRIVER_H_

#include <stdint.h>


//// ���� ��� ����������� � W5500 (����� ��� SCLK, MOSI, MISO)
// CS
#define W5500_CS_PORT		PORTC
#define W5500_CS_DDR		DDRC
#define W5500_CS_PIN		(1 << 0)
// RESET
#define W5500_RESET_PORT	PORTB
#define W5500_RESET_DDR	        DDRB
#define W5500_RESET_PIN		(1 << 2)
// INT
//#define W5500_INT_PORT 	PORTC
//#define W5500_INT_DDR 	DDRC
//#define W5500_INT_PIN		(1 << 1)


void W5500_Init(void);
void W5500_chipInit(void);
void W5500WriteByte(unsigned char byte);
unsigned char W5500ReadByte(void);
void W5500Select(void);
void W5500DeSelect(void);
uint8_t wizchip_read(void);
void  wizchip_write(uint8_t wb);

#endif
