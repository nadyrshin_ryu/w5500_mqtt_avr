//------------------------------------------------------------------------------
// This is Open source software. You can place this code on your site, but don't
// forget a link to my YouTube-channel: https://www.youtube.com/channel/UChButpZaL5kUUl_zTyIDFkQ
// ��� ����������� ����������� ���������������� ��������. �� ������ ���������
// ��� �� ����� �����, �� �� �������� ������� ������ �� ��� YouTube-����� 
// "����������� � ���������" https://www.youtube.com/channel/UChButpZaL5kUUl_zTyIDFkQ
// �����: �������� ������ / Nadyrshin Ruslan
//------------------------------------------------------------------------------
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <dhcp/dhcp.h>
#include <dns/dns.h>
#include <wizchip_conf.h>
#include <mqtt_interface.h>
#include <MQTTClient.h>
#include <W5500_driver.h>
#include <w5500.h>
#include <timers.h>
#include <terminal_io.h>
#include <net_config.h>


// SOCKET NUMBER DEFINION for Examples
#define SOCK_TCPS       0
#define SOCK_UDPS       1
#define SOCK_DHCP	7
// Receive Buffer
#define BUFFER_SIZE	512     // 2048
uint8_t tempBuffer[BUFFER_SIZE];
// Global variables
uint32_t dhcp_counter;
uint8_t mqtt_push_counter;
uint8_t mqtt_flag;
uint16_t mes_id;

wiz_NetInfo gWIZNETINFO = 
{
  .mac = {ETHADDR0, ETHADDR1, ETHADDR2, ETHADDR3, ETHADDR4, ETHADDR5},
  .ip = {IPADDR0, IPADDR1, IPADDR2, IPADDR3},
  .sn = {NETMASK0, NETMASK1, NETMASK2, NETMASK3},
  .gw = {DRIPADDR0, DRIPADDR1, DRIPADDR2, DRIPADDR3},
  .dns = {DRIPADDR0, DRIPADDR1, DRIPADDR2, DRIPADDR3},
  .dhcp = NETINFO_DHCP  // NETINFO_STATIC
};


//==============================================================================
// ���������� ���������� �� ������� (������ = 1 ��)
//==============================================================================
void msTick_Handler(void) 
{
  MilliTimer_Handler();

  if (++dhcp_counter >= 1000)  // ������ ������� 
  {
    dhcp_counter = 0;
    DHCP_time_handler(); // ������� ��� ��������� ����-����� DHCP

    if (++mqtt_push_counter >= 10)  // ������ 10 ������
    {
      mqtt_push_counter = 0;
      mqtt_flag = 1;
    }
  }
}
//==============================================================================


//==============================================================================
// ��������� ������� ������� ���������
//==============================================================================
void print_network_information(void)
{
  wizchip_getnetinfo(&gWIZNETINFO);
  printf("Mac address: %02x:%02x:%02x:%02x:%02x:%02x\n\r", gWIZNETINFO.mac[0], gWIZNETINFO.mac[1], gWIZNETINFO.mac[2], gWIZNETINFO.mac[3], gWIZNETINFO.mac[4], gWIZNETINFO.mac[5]);
  if (gWIZNETINFO.dhcp == NETINFO_DHCP)
    printf("DHCP\n\r");
  else
    printf("Static IP\n\r");
  printf("IP address : %d.%d.%d.%d\n\r", gWIZNETINFO.ip[0], gWIZNETINFO.ip[1], gWIZNETINFO.ip[2], gWIZNETINFO.ip[3]);
  printf("SM Mask    : %d.%d.%d.%d\n\r", gWIZNETINFO.sn[0], gWIZNETINFO.sn[1], gWIZNETINFO.sn[2], gWIZNETINFO.sn[3]);
  printf("Gate way   : %d.%d.%d.%d\n\r", gWIZNETINFO.gw[0], gWIZNETINFO.gw[1], gWIZNETINFO.gw[2], gWIZNETINFO.gw[3]);
  printf("DNS Server : %d.%d.%d.%d\n\r", gWIZNETINFO.dns[0], gWIZNETINFO.dns[1], gWIZNETINFO.dns[2], gWIZNETINFO.dns[3]);
}
//==============================================================================


//==============================================================================
// ���������� ��������� MQTT, ��������������� ��������� �������
//==============================================================================
void messageArrived(MessageData* md)
{
  MQTTMessage* message = md->message;
  /*
  for (uint8_t i = 0; i < md->topicName->lenstring.len; i++)
    putchar(*(md->topicName->lenstring.data + i));

  printf(" (%.*s)\r\n", (int32_t)message->payloadlen, (char*)message->payload);
  */
}
//==============================================================================


//==============================================================================
// ������� ����������� �������� DHCP � ������ IP ���������� ��� �������������
//==============================================================================
uint8_t DHCP_proc(void)
{
  uint8_t dhcp_res = DHCP_run();
  switch (dhcp_res)
  {
  case DHCP_IP_ASSIGN:
  case DHCP_IP_CHANGED:
  case DHCP_IP_LEASED:
    getIPfromDHCP(gWIZNETINFO.ip);
    getGWfromDHCP(gWIZNETINFO.gw);
    getSNfromDHCP(gWIZNETINFO.sn);
    getDNSfromDHCP(gWIZNETINFO.dns);
    gWIZNETINFO.dhcp = NETINFO_DHCP;
    ctlnetwork(CN_SET_NETINFO, (void*)&gWIZNETINFO);
#ifdef _DHCP_DEBUG_
    printf("\r\n>> DHCP IP Leased Time : %ld Sec\r\n", getDHCPLeasetime());
#endif
    break;
  case DHCP_FAILED:
#ifdef _DHCP_DEBUG_
    printf(">> DHCP Failed\r\n");
#endif
    gWIZNETINFO.dhcp = NETINFO_STATIC;
    break;
  }
  return dhcp_res;
}
//==============================================================================


//==============================================================================
// ������� ���������� ��������������� ������ � ����� StrBuff
//==============================================================================
int8_t str_printf(char *StrBuff, uint8_t BuffLen, const char *args, ...)
{
  va_list ap;
  va_start(ap, args);
  int8_t len = vsnprintf(StrBuff, BuffLen, args, ap);
  va_end(ap);
  return len;
}
//==============================================================================


//==============================================================================
// MAIN
//==============================================================================
int32_t main(void)
{
  int32_t rc = 0;
  uint8_t dhcp_ret = DHCP_STOPPED;
  uint8_t buf[100];
      

  W5500_Init();
  tmr2_init(1000, msTick_Handler);
  tmr2_start();
  W5500_chipInit();

  // Set network informations
  wizchip_setnetinfo(&gWIZNETINFO);
  setSHAR(gWIZNETINFO.mac);

  // �������� ���� IP �� DHCP-�������, ���� ������� ��������������� �����
  if (gWIZNETINFO.dhcp == NETINFO_DHCP)
  {
    DHCP_init(SOCK_DHCP, tempBuffer);
  
    while (!((dhcp_ret == DHCP_IP_ASSIGN) || (dhcp_ret == DHCP_IP_CHANGED) || (dhcp_ret == DHCP_FAILED) || (dhcp_ret == DHCP_IP_LEASED)))
      dhcp_ret = DHCP_proc();
  }
  
  print_network_information();

  // ���� ���� �������, ������������� ���������� � ���
  Network n;
  Client c;
  n.my_socket = 0;

  uint8_t targetIP[4] = {192, 168, 1, 35};      // IP ������� MQTT
  
  // ����� ���������� IP ���� �� DNS-�����, IP ���� ����� � ������� targetIP
  //DNS_init(1, tempBuffer);
  //DNS_run(gWIZNETINFO.dns, "test.mosquitto.org", targetIP);

  NewNetwork(&n);
  ConnectNetwork(&n, targetIP, 1883);
  MQTTClient(&c, &n, 1000, buf, 100, tempBuffer, BUFFER_SIZE);

  // ����������� � ������� MQTT
  MQTTPacket_connectData data = MQTTPacket_connectData_initializer;
  data.willFlag = 0;
  data.MQTTVersion = 4;//3;
  data.clientID.cstring = (char*)"w5500-client";
  data.username.cstring = "username";
  data.password.cstring = "";
  data.keepAliveInterval = 60;
  data.cleansession = 1;
  rc = MQTTConnect(&c, &data);
  printf("Connected %d\r\n", rc);
  
  // ������������� �� �����
  char SubString[] = "/#";
  rc = MQTTSubscribe(&c, SubString, QOS0, messageArrived);
  printf("Subscribed (%s) %d\r\n", SubString, rc);

  while(1)
  {
    // ��������� ������� DHCP
    if (gWIZNETINFO.dhcp == NETINFO_DHCP)
      dhcp_ret = DHCP_proc();
    
    // ������������� �������� ������ �� MQTT
    if (mqtt_flag)
    {
      mqtt_flag = 0;

      char message[16];
      int8_t len = str_printf(message, sizeof(message), "%d.%d.%d.%d", gWIZNETINFO.ip[0], gWIZNETINFO.ip[1], gWIZNETINFO.ip[2], gWIZNETINFO.ip[3]);
      if (len > 0)
      {
        MQTTMessage pubMessage;
        pubMessage.qos = QOS0;
        pubMessage.id = mes_id++;
        pubMessage.payloadlen = len;
        pubMessage.payload = message;
        MQTTPublish(&c, "/w5500_avr_client", &pubMessage);
      }
    }
    
    // ���� ������� � ����������� ���������� � ��������
    MQTTYield(&c, 1000);
  }
}
//==============================================================================

